if($('.section-other-project .swiper-container').length){
    var mySwiper2 = new Swiper('.section-other-project .swiper-container', {
        slidesPerView: 3,
        slidesPerGroup: 3,
        spaceBetween: 70,
        loop: true,
        loopFillGroupWithBlank: 3,
        // centeredSlides: true,
        pagination: {
            el: '.section-other-project .swiper-pagination',
            clickable: true
        },
        breakpoints: {
            992: {
              slidesPerView: 2,
              spaceBetween: 40,
              slidesPerGroup: 2
            },
            600: {
              slidesPerView: 1,
              slidesPerGroup: 1
            }
        }
    });
}

$('.animated').viewportChecker({
    classToAdd: 'show',
    offset: 100,
    invertBottomOffset: true,
});
$('.animate-top').addClass('hidden').viewportChecker({
    classToAdd: 'animated fadeInUp',
    classToRemove: 'hidden',
    classToAddForFullView: 'full-visible',
    offset: 100,
    invertBottomOffset: true,
    callbackFunction: function(elem, action){
        var screen = $(elem).find('.screen-bg');
        if(screen.is('video')){
            setTimeout(function(){
                screen[0].play();
            }, 1500)
        }
    }
});
$('.animate-left').viewportChecker({
    classToAdd: 'animated to-left',
    offset: 100,
    invertBottomOffset: true,
    callbackFunction: function(elem, action){
        var screen = $(elem).find('.screen-bg');
        if(screen.is('video')){
            setTimeout(function(){
                screen[0].play();
            }, 1500)
        }
    }
});
$('.animate-right').viewportChecker({
    classToAdd: 'animated to-right',
    offset: 100,
    invertBottomOffset: true,
    callbackFunction: function(elem, action){
        var screen = $(elem).find('.screen-bg');
        if(screen.is('video')){
            setTimeout(function(){
                screen[0].play();
            }, 1500)
        }
    }
});

$('.animate-to-left-slowly').viewportChecker({
    classToAdd: 'to-left-slowly',
    offset: 100,
    invertBottomOffset: true,
});

$('.animate-to-right-slowly').viewportChecker({
    classToAdd: 'to-right-slowly',
    offset: 100,
    invertBottomOffset: true,
});
$('.animate-to-top-phone').viewportChecker({
    classToAdd: 'to-top-phone',
    offset: -300,
    invertBottomOffset: true,
});



$('.sandwitch-icon').on('click', function() {
    $(this).parent().toggleClass('show');
});


function checkScroll() {
    if ($(window).scrollTop()) {
        $('header').addClass('scrolled');
    } else {
        $('header').removeClass('scrolled');
    }
}

checkScroll();

$(window).on('scroll', checkScroll);

if($('svg.scheme').length){
    var width = $(window).width()/2;
    var height = $(window).height()/2;
    $('svg.scheme .left-eye, svg.scheme .right-eye').each(function(event){
        var eye = $(this);
        $(document).on('mousemove', function(e){
            var eyePos = eye.position();
            var bySum = 2.4;
            var byX = (e.pageX - eyePos.left) / width;
            var byY = (e.pageY - eyePos.top) / height;
            if(byX > 1){
                byX = 1;
            }
            if(byY > 1){
                byY = 1;
            }
            var sum = byX + byY;
            if(sum > 1){
                var koef = 1 / sum;
                byX = byX * koef;
                byY = byY * koef;
            }
            eye.css('transform', 'translate(' + byX * bySum + 'px, ' + byY * bySum + 'px)')
        })
    })
}

window.onload = function(){
	$('.section-can-help .swiper-container').each(function(){
		let maxHeight = 0;
		$(this).find('.swiper-slide .inner').each(function(){
			if(maxHeight < $(this).outerHeight()){
				maxHeight = $(this).outerHeight();
			}
		});
		$(this).height(maxHeight);
	});
    $('.with-phones').addClass('show');
    setTimeout(function(){
        $('.with-phones').addClass('full-visible');
    }, 3300)
}

$('.field input, .field textarea').blur(function(){
    if(!$(this).val()){
        $(this).closest('.field').removeClass('has-focus');
    }
})

$('.field input, .field textarea').focus(function(){
    $(this).closest('.field').addClass('has-focus');
});

$('.btn-scroll').on('click', function(){
    $('html, body').animate({
        scrollTop: $('.section-can-help').offset().top - $('header').height()
     }, 800);
});

$('.btn-submit').on('click', function(e){
    var form = $(this).closest('form');
    var inputs = form.find('[required]');
    form.find('.field').removeClass('error');
    var error = false;
    inputs.each(function(){
        if(validateInput($(this))){
            error = true;
        }
        
    });
    if(error){
        e.preventDefault();
    }
});
$('form [required]').each(function(){
    $(this).blur(function(){
        $(this).closest('.field').removeClass('error');
        validateInput($(this));
    })
});

function validateInput(input){
    var error = false;
    var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(!input.val()){
        input.closest('.field').addClass('error');
        error = true;
    }
    if(input.attr('type') === "email" && !emailRegex.test(input.val())){
        input.closest('.field').addClass('error');
        error = true;
    }
    return error;
}

$('.section-tabs .blocks .block').on('click', function(){
    $(this).addClass('active').siblings().removeClass('active');
    $(this).closest('.section-tabs').find(`[data-image="${$(this).data('block')}"]`).addClass('active').siblings().removeClass('active');
});

$('header .sandwitch-btn').on('click', function(){
    $('body').toggleClass('show-menu');
})